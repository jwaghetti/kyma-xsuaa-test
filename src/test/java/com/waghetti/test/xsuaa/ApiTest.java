package com.waghetti.test.xsuaa;

import com.waghetti.test.xsuaa.authorization.AuthorizationTestDouble;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import spark.Spark;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class ApiTest {

  private static HttpClient httpClient;

  @BeforeAll
  static void beforeAll() {
    AuthorizationTestDouble.registerAuthorizationStub();
    Spark.port(80);
    Main.setupApi();
    Spark.awaitInitialization();
    httpClient = HttpClients.createDefault();
  }

  @AfterAll
  static void afterAll() {
    AuthorizationTestDouble.removeMock();
    Spark.awaitStop();
  }

  @Test
  void testHello() throws IOException  {
    HttpGet request = new HttpGet("http://localhost/hello");
    HttpResponse response = httpClient.execute(request);
    assertEquals("Hello World!", EntityUtils.toString(response.getEntity()));
  }


  @Test
  void testName() throws IOException {
    HttpGet request = new HttpGet("http://localhost/hello/jiba");
    HttpResponse response = httpClient.execute(request);
    assertEquals("Hello jiba!", EntityUtils.toString(response.getEntity()));
  }

  @Test
  void testNoAuthCounter() throws IOException {
    HttpPost request = new HttpPost("http://localhost/unprotectedcount");
    httpClient.execute(request);
    HttpResponse response = httpClient.execute(request);
    assertEquals("2", EntityUtils.toString(response.getEntity()));
  }

  @Test
  void testCounter() throws IOException {
    HttpPost request = new HttpPost("http://localhost/count");
    request.addHeader("Authorization", "token");
    HttpResponse response = httpClient.execute(request);
    assertEquals("1", EntityUtils.toString(response.getEntity()));
  }

  @Test
  void testWrongTokenCounter() throws IOException {
    HttpPost request = new HttpPost("http://localhost/count");
    request.addHeader("Authorization", "invalidtoken");
    HttpResponse response = httpClient.execute(request);
    assertEquals(404, response.getStatusLine().getStatusCode());
  }

}
