package com.waghetti.test.xsuaa.authorization;

public class AuthorizationTestDouble {

  public static void registerAuthorizationStub() {
   AuthorizationHandlerFactory.setHandler(new AuthorizationHandlerImpl());
  }

  public static void removeMock() {
    AuthorizationHandlerFactory.setHandler(null);
  }

  public static class AuthorizationHandlerImpl implements AuthorizationHandler {

    @Override
    public boolean authorize(String token) {
      return "token".equals(token);
    }

  }

}
