package com.waghetti.test.xsuaa.api;

public class ApiHello {

  public String handle(String name) {

    if (name == null || "".equals(name)) {
      return "Hello World!";
    }

    return "Hello " + name + "!";

  }

}
