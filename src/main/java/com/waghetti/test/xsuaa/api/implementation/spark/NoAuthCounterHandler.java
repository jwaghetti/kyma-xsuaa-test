package com.waghetti.test.xsuaa.api.implementation.spark;

import com.waghetti.test.xsuaa.api.ApiCounter;

import spark.Request;
import spark.Response;
import spark.Route;


public class NoAuthCounterHandler implements Route {

  private ApiCounter apiObject = new ApiCounter();

  @Override
  public Object handle(Request request, Response response) throws Exception {
    response.status(200);
    return apiObject.handle();
  }

}
