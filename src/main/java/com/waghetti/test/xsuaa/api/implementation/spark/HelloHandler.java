package com.waghetti.test.xsuaa.api.implementation.spark;

import com.waghetti.test.xsuaa.api.ApiHello;
import spark.Request;
import spark.Response;
import spark.Route;

public class HelloHandler implements Route {

  private ApiHello apiObject = new ApiHello();

    @Override
    public Object handle(Request request, Response response) throws Exception {

      response.status(200);
      return apiObject.handle(request.params(":name"));

    }

  }
