package com.waghetti.test.xsuaa.api;

public class ApiCounter {

  private int counter = 0;

  public String handle() {
    return Integer.toString(++counter);
  }

}
