package com.waghetti.test.xsuaa.api.implementation.spark;

import com.waghetti.test.xsuaa.api.ApiCounter;
import com.waghetti.test.xsuaa.authorization.AuthorizationHandlerFactory;

import spark.Request;
import spark.Response;
import spark.Route;

public class CounterHandler implements Route {

  ApiCounter apiObject = new ApiCounter();

    @Override
    public Object handle(Request request, Response response) throws Exception {

      if (AuthorizationHandlerFactory.getHandler().authorize(request.headers("Authorization"))) {
        response.status(200);
        return apiObject.handle();
      } else {
        response.status(401);
        return null;
      }

    }

  }
