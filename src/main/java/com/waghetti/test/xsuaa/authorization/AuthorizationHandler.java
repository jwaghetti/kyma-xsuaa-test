package com.waghetti.test.xsuaa.authorization;

public interface AuthorizationHandler {

  boolean authorize(String token);

}
