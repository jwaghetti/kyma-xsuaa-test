package com.waghetti.test.xsuaa.authorization;

import com.waghetti.test.xsuaa.authorization.xsuaa.XsuaaAuthorizationHandler;

public class AuthorizationHandlerFactory {

  private static AuthorizationHandler handler;

  protected static void setHandler(AuthorizationHandler handler) {
    AuthorizationHandlerFactory.handler = handler;
  }

  public static AuthorizationHandler getHandler() {

    if (handler == null) {
      handler = new XsuaaAuthorizationHandler();
    }

    return handler;

  }

}
