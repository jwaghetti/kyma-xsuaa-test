package com.waghetti.test.xsuaa.authorization.xsuaa;

import com.sap.cloud.security.config.OAuth2ServiceConfiguration;
import com.sap.cloud.security.config.OAuth2ServiceConfigurationBuilder;
import com.sap.cloud.security.config.Service;
import com.sap.cloud.security.config.cf.CFConstants;
import com.sap.cloud.security.token.Token;
import com.sap.cloud.security.token.validation.CombiningValidator;
import com.sap.cloud.security.token.validation.ValidationResult;
import com.sap.cloud.security.token.validation.validators.JwtValidatorBuilder;
import com.waghetti.test.xsuaa.authorization.AuthorizationHandler;

public class XsuaaAuthorizationHandler implements AuthorizationHandler {

  @Override
  public boolean authorize(String token) {

    String clientId = System.getenv("SECRET_XSUAA_CLIENTID");
    String clientSecret = System.getenv("SECRET_XSUAA_CLIENTSECRET");
    String xsuaaUrl = System.getenv("SECRET_XSUAA_URL");

    OAuth2ServiceConfiguration serviceConfig = OAuth2ServiceConfigurationBuilder.forService(Service.XSUAA)
        .withProperty(CFConstants.XSUAA.APP_ID, "garouter-uaa")
        .withProperty(CFConstants.XSUAA.UAA_DOMAIN, "authentication.eu10.hana.ondemand.com")
        //.withUrl(xsuaaUrl)
        .withClientId(clientId)
        //.withClientSecret(clientSecret)
        .build();

    CombiningValidator<Token> validators = JwtValidatorBuilder.getInstance(serviceConfig).build();

    Token authorizationHeader = Token.create(token);

    ValidationResult result = validators.validate(authorizationHeader);

    return result.isValid();
  }
}
