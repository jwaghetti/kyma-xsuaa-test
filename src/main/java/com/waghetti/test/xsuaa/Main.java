package com.waghetti.test.xsuaa;

import com.waghetti.test.xsuaa.api.implementation.spark.CounterHandler;
import com.waghetti.test.xsuaa.api.implementation.spark.HelloHandler;
import com.waghetti.test.xsuaa.api.implementation.spark.NoAuthCounterHandler;

import static spark.Spark.get;
import static spark.Spark.post;


public class Main {

  public static volatile int counter = 0;

  public static void main(String[] args) {
    setupApi();
  }

  public static void setupApi() {


    HelloHandler helloHandler = new HelloHandler();

    get("/hello", helloHandler);
    get("/hello/:name", helloHandler);

    post("/count", new CounterHandler());
    post("/unprotectedcount", new NoAuthCounterHandler());

  }

}
