FROM openjdk:11

COPY target/xsuaa-test*-with-dependencies.jar /app/xsuaa-test.jar

WORKDIR /app

CMD ["java", "-jar", "xsuaa-test.jar"]